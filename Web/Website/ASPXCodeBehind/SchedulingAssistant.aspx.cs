﻿/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Data;
using DevExpress.Web.ASPxGridView;
using System.Collections;

namespace ns_MyVRM
{
    public partial class SchedulingAssistant : System.Web.UI.Page
    {
        #region Protected Variables

        protected System.Web.UI.WebControls.Table tblUsrList;
        protected System.Web.UI.WebControls.Table tblTimeSlot;
        protected System.Web.UI.WebControls.Table tblHeaderSlot;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpnConfDate;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnConfStartHour;

        #endregion

        #region private variable

        ns_Logger.Logger log;
        myVRMNet.NETFunctions obj;
        DateTime ConfStartDate = new DateTime();
        DateTime ConfEndDate = new DateTime();
        protected String format = "MM/dd/yyyy";
        string ConfStDate = "";
        string ConfEndDateVal = "";
        DataTable rptTable = new DataTable();
        string partyList = "", partyName = "";
        string ConfDate = DateTime.Now.Date.ToString();
        int tRowVal = 13;
        TableRow tRow = null;
        TableCell tCell = null;

        #endregion

       

        #region Constructor
        public SchedulingAssistant()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }
        #endregion

        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        #region Page_Load
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();

               if(!IsPostBack)
               {
                   if (Request.QueryString["stDate"] != null)
                   {
                       if (Request.QueryString["stDate"] != "")
                           ConfStDate = Request.QueryString["stDate"].ToString();
                   }
                   if (Request.QueryString["enDate"] != null)
                   {
                       if (Request.QueryString["enDate"] != "")
                           ConfEndDateVal = Request.QueryString["enDate"].ToString();
                   }

                   if (Session["FormatDateType"] != null)
                   {
                       if (Session["FormatDateType"].ToString() != "")
                           format = Session["FormatDateType"].ToString();
                   }

                   if (ConfStDate != "")
                       ConfStartDate = Convert.ToDateTime(ConfStDate);

                   hdnConfStartHour.Value = ConfStartDate.ToString("HH:mm").Split(':')[0];

                   if (ConfEndDateVal != "")
                       ConfEndDate = Convert.ToDateTime(ConfEndDateVal);

                   SpnConfDate.InnerText = ConfStartDate.Date.ToLongDateString();

                   if (ConfStDate != "")
                       ConfDate = ConfStDate.Split(' ')[0];

                   if (Session["PartyList"] != null)
                   {
                       if (Session["PartyList"].ToString() != "")
                           partyList = Session["PartyList"].ToString();
                   }

                   string inXML = "<GetPartyConferenceDetails><loginID>" + Session["userID"] + "</loginID>" + obj.OrgXMLElement() + "<Partylist>" + partyList + "</Partylist><Confdate>" + ConfDate + "</Confdate></GetPartyConferenceDetails>";

                   string outXML = obj.CallMyVRMServer("GetUserAvailability", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                   //string outXML = "<GetPartyConferenceDetails><PartyConferenceDetails><PartyName>User Admin </PartyName><PartyEmail>useradmin@whizbangwidget.com</PartyEmail><Confdetails><ConfStartDate>05/26/2016 06:00 PM</ConfStartDate><ConfEndDate>05/26/2016 07:00 PM</ConfEndDate><PartyFreeBusyStatus>2</PartyFreeBusyStatus></Confdetails><Confdetails><ConfStartDate>05/26/2016 07:00 PM</ConfStartDate><ConfEndDate>05/26/2016 08:00 PM</ConfEndDate><PartyFreeBusyStatus>2</PartyFreeBusyStatus></Confdetails></PartyConferenceDetails><PartyConferenceDetails><PartyName>myVRM Admin </PartyName><PartyEmail>admin@myvrm.sandbox.vidyo.com</PartyEmail><Confdetails><ConfStartDate>05/26/2016 06:00 PM</ConfStartDate><ConfEndDate>05/26/2016 07:00 PM</ConfEndDate><PartyFreeBusyStatus>2</PartyFreeBusyStatus></Confdetails><Confdetails><ConfStartDate>05/26/2016 07:00 PM</ConfStartDate><ConfEndDate>05/26/2016 08:00 PM</ConfEndDate><PartyFreeBusyStatus>2</PartyFreeBusyStatus></Confdetails></PartyConferenceDetails><PartyConferenceDetails><PartyName>Org Admin1 </PartyName><PartyEmail>orgadmin1@mothergoose.us</PartyEmail><Confdetails><ConfStartDate>05/26/2016 06:00 PM</ConfStartDate><ConfEndDate>05/26/2016 07:00 PM</ConfEndDate><PartyFreeBusyStatus>2</PartyFreeBusyStatus></Confdetails><Confdetails><ConfStartDate>05/26/2016 07:00 PM</ConfStartDate><ConfEndDate>05/26/2016 08:00 PM</ConfEndDate><PartyFreeBusyStatus>2</PartyFreeBusyStatus></Confdetails></PartyConferenceDetails></GetPartyConferenceDetails>";

                   BindData(outXML);

                   ScriptManager.RegisterStartupScript(this, this.GetType(), "DataLoadingDft3", "DataLoadingnew(0);", true);                       
               }
            }
            catch (Exception ex)
            {
                log.Trace("Page_Load" + ex.Message);
            }
        }
        #endregion

        #region BindData

        private void BindData(string outXML)
        {
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                XmlNodeList nodes = null;
                XmlNodeList subNodes = null;

                double StartDateDiff = 0.0, EndDateDiff = 0.0, DurationDiff = 0.0;
                string xWidth = "", xLeft = "", PartyConfStart = "", PartyConfEnd = "", PartyFreeBusyStatus = "";
                bool bGrayDiv = false, OverlapDiv = false;
                string FreeBusyStatus = "0";
                int status = 0;

                DateTime PartyConfStDate = new DateTime();
                DateTime PartyConfEndDate = new DateTime();
               
                HtmlGenericControl DivControl = new HtmlGenericControl("div");
                HtmlGenericControl PartyFreeBusyDivControl = new HtmlGenericControl("div");

                TimeSpan durationVal = ConfEndDate.Subtract(ConfStartDate);

                if (outXML.IndexOf("<error>") < 0 && outXML != "")
                {
                    xmldoc.LoadXml(outXML);
                    nodes = xmldoc.SelectNodes("//GetPartyConferenceDetails/PartyConferenceDetails");
                    if (nodes != null && nodes.Count > 13)
                        tRowVal = nodes.Count;
                }
                Int32 start = -1, end = -1;
                string startGreyWidth = "", endGreyWidth = "", startLeft = "";

                for (int x = 0; x < tRowVal; x++)
                {
                    Hashtable partyDetails = new Hashtable();

                    if (nodes != null && nodes[x] != null)
                    {
                        subNodes = nodes[x].SelectNodes("Confdetails");

                        for (int j = 0; j < subNodes.Count; j++)
                        {
                            if (subNodes[j] != null)
                            {
                                if (subNodes[j].SelectSingleNode("ConfStartDate") != null)
                                    PartyConfStart = subNodes[j].SelectSingleNode("ConfStartDate").InnerText;

                                if (subNodes[j].SelectSingleNode("ConfEndDate") != null)
                                    PartyConfEnd = subNodes[j].SelectSingleNode("ConfEndDate").InnerText;

                                if (subNodes[j].SelectSingleNode("PartyFreeBusyStatus") != null)
                                    PartyFreeBusyStatus = subNodes[j].SelectSingleNode("PartyFreeBusyStatus").InnerText;

                                PartyConfStDate = Convert.ToDateTime(PartyConfStart);
                                PartyConfEndDate = Convert.ToDateTime(PartyConfEnd);

                                PartyConfStDate = Convert.ToDateTime(PartyConfStart);
                                PartyConfEndDate = Convert.ToDateTime(PartyConfEnd);

                                DateTime tempDate = PartyConfStDate;
                                if (PartyConfStDate.Minute != 0 || PartyConfEndDate.Minute != 0)
                                {
                                    for (Int32 h = PartyConfStDate.Hour; h <= PartyConfEndDate.Hour; h++)
                                    {
                                        if (!partyDetails.ContainsKey(h))
                                            partyDetails.Add(h, PartyConfStart + "|" + PartyConfEnd + "|" + PartyFreeBusyStatus);
                                    }
                                }
                                else
                                {
                                    if (!partyDetails.ContainsKey(PartyConfStDate.Hour))
                                        partyDetails.Add(PartyConfStDate.Hour, PartyConfStart + "|" + PartyConfEnd + "|" + PartyFreeBusyStatus);
                                }
                            }
                        }
                    }

                    #region Bind User Time Slots

                    tRow = new TableRow();
                    for (int i = 0; i < 24; i++)
                    {
                        StartDateDiff = 0.0; EndDateDiff = 0.0; DurationDiff = 0.0;
                        xWidth = ""; xLeft = ""; bGrayDiv = false; 
                        PartyConfStart = ""; PartyConfEnd = "";
                        OverlapDiv = false;
                        DivControl = new HtmlGenericControl("div");

                        tCell = new TableCell();
                        tCell.Text = "";
                        tCell.Style.Add("height", "17px");

                        #region Conference Date and Time Gray Color
                        if (x == 0 && (start == -1 || end == -1)) // One time calculation for active time slot
                        {
                            StartDateDiff = ((double)ConfStartDate.Minute / 60.0) * 100.0;
                            EndDateDiff = ((double)ConfEndDate.Minute / 60.0) * 100.0;
                            DurationDiff = ((double)durationVal.TotalMinutes / 60.0) * 100.0;

                            if (ConfStartDate.Hour == i && ConfEndDate.Hour != i)
                            {
                                xLeft = StartDateDiff.ToString() + "%";
                                xWidth = (100 - StartDateDiff) + "%";

                                DivControl.Style.Add("border-left", "3px solid green");
                                bGrayDiv = true;
                            }
                            else if (ConfStartDate.Hour != i && ConfEndDate.Hour == i)
                            {
                                xWidth = EndDateDiff.ToString() + "%";

                                DivControl.Style.Add("border-right", "3px solid red");
                                bGrayDiv = true;
                            }
                            else if (ConfStartDate.Hour == i && ConfEndDate.Hour == i)
                            {
                                xLeft = StartDateDiff.ToString() + "%";
                                xWidth = (EndDateDiff - StartDateDiff) + "%";

                                DivControl.Style.Add("border-left", "3px solid green");
                                DivControl.Style.Add("border-right", "3px solid red");
                                bGrayDiv = true;
                            }
                            else if (ConfStartDate.Hour < i && ConfEndDate.Hour > i)
                            {
                                xWidth = "100%";
                                bGrayDiv = true;
                            }

                            if (bGrayDiv == true)
                            {
                                if (start == -1)
                                {
                                    start = i;
                                    startGreyWidth = xWidth;
                                    startLeft = xLeft;
                                }
                                else
                                {
                                    end = i;
                                    endGreyWidth = xWidth;
                                }
                            }
                        }

                        if(start == i || end == i)
                        {
                            bGrayDiv = true;
                            if (startGreyWidth != "" && start == i)
                            {
                                DivControl.Style.Add("width", startGreyWidth);
                                DivControl.Style.Add("border-left", "3px solid green");
                            }
                            
                            if (endGreyWidth != "" && end == i)
                            {
                                DivControl.Style.Add("width", endGreyWidth);
                                DivControl.Style.Add("border-right", "3px solid red");
                            }
                            if (startLeft != "" && start == i)
                                DivControl.Style.Add("left", startLeft);

                            DivControl.Attributes.Add("class", "ConfBgColor");
                            tCell.Controls.Add(DivControl);
                        }
                        #endregion

                        #region Bind Participant FreeBusy Status

                        if (partyDetails.ContainsKey(i))
                        {
                            PartyConfStart = partyDetails[i].ToString().Split('|')[0];
                            PartyConfEnd = partyDetails[i].ToString().Split('|')[1];
                            PartyFreeBusyStatus = partyDetails[i].ToString().Split('|')[2];

                            #region PartyConfFreeBusy

                            if (PartyConfStart != "" && PartyConfEnd != "")
                            {
                                PartyConfStDate = Convert.ToDateTime(PartyConfStart);
                                PartyConfEndDate = Convert.ToDateTime(PartyConfEnd);

                                TimeSpan duration = PartyConfEndDate.Subtract(PartyConfStDate);

                                StartDateDiff = 0.0; EndDateDiff = 0.0; DurationDiff = 0.0;
                                xWidth = ""; xLeft = "";
                                bool bPartyDiv = false;
                                PartyFreeBusyDivControl = new HtmlGenericControl("div");

                                StartDateDiff = ((double)PartyConfStDate.Minute / 60.0) * 100.0;
                                EndDateDiff = ((double)PartyConfEndDate.Minute / 60.0) * 100.0;
                                DurationDiff = ((double)duration.TotalMinutes / 60.0) * 100.0;

                                if (PartyConfStDate.Hour == i && PartyConfEndDate.Hour != i)
                                {
                                    xWidth = (100 - StartDateDiff) + "%";
                                    xLeft = StartDateDiff.ToString() + "%";
                                    bPartyDiv = true;
                                }
                                else if (PartyConfStDate.Hour != i && PartyConfEndDate.Hour == i)
                                {
                                    xWidth = EndDateDiff.ToString() + "%";
                                    bPartyDiv = true;
                                }
                                else if (PartyConfStDate.Hour == i && PartyConfEndDate.Hour == i)
                                {
                                    xWidth = (EndDateDiff - StartDateDiff) + "%";
                                    xLeft = StartDateDiff.ToString() + "%";
                                    bPartyDiv = true;
                                }
                                else if (PartyConfStDate.Hour < i && PartyConfEndDate.Hour > i)
                                {
                                    xWidth = "100%";
                                    bPartyDiv = true;
                                }

                                if (bPartyDiv == true)
                                {
                                    if (xWidth != "" && xWidth != "0%")
                                    {
                                        PartyFreeBusyDivControl.Style.Add("width", xWidth);
                                        PartyFreeBusyDivControl.Attributes.Add("class", "PartyFreeBusyCells");

                                        if (xLeft != "")
                                            PartyFreeBusyDivControl.Style.Add("left", xLeft);

                                        if (bGrayDiv == true || OverlapDiv == true)
                                            PartyFreeBusyDivControl.Style.Add("margin-top", "-17px");

                                        if (PartyFreeBusyStatus != null)
                                            FreeBusyStatus = PartyFreeBusyStatus;

                                        Int32.TryParse(FreeBusyStatus, out status);

                                        switch (status)
                                        {
                                            case (int)myVRMNet.NETFunctions.LegacyFreeBusyStatus.Busy:
                                                PartyFreeBusyDivControl.Style.Add("background-color", "#354FFC");
                                                break;
                                            case (int)myVRMNet.NETFunctions.LegacyFreeBusyStatus.Tentative:
                                                PartyFreeBusyDivControl.Style.Add("background-color", "orangered");
                                                break;
                                            case (int)myVRMNet.NETFunctions.LegacyFreeBusyStatus.OOF:
                                                PartyFreeBusyDivControl.Style.Add("background-color", "#71046D");
                                                break;
                                            case (int)myVRMNet.NETFunctions.LegacyFreeBusyStatus.NoData:
                                                PartyFreeBusyDivControl.Style.Add("background-color", "#FDFDFD");
                                                break;
                                            case (int)myVRMNet.NETFunctions.LegacyFreeBusyStatus.WorkingElsewhere:
                                                PartyFreeBusyDivControl.Style.Add("background-color", "gray");
                                                break;
                                            default:
                                                break;
                                        }

                                        tCell.Controls.Add(PartyFreeBusyDivControl);
                                        OverlapDiv = true;
                                    }
                                }
                            }
                            #endregion
                        }
                        #endregion

                        tRow.Cells.Add(tCell);
                    }
                    tblTimeSlot.Controls.Add(tRow);
                    #endregion
                }
            }
            catch (Exception ex)
            {
                log.Trace("BindData" + ex.Message);
            }
        }
        #endregion

    }
}