﻿<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>

<%@ Page Language="C#" Inherits="ns_MyVRM.SchedulingAssistant" Buffer="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />

<script type="text/javascript">
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css";
    document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>");

    function DataLoadingnew(val) {       
        //        getMouseXY();
        if (parent.document.getElementById("dataLoadingDIV") == null)
            return false;
        //document.getElementById("dataLoadingDIV").style.position = 'absolute'; //FB 2814
        parent.document.getElementById("dataLoadingDIV").style.left = window.screen.width / 2 - 100;
        parent.document.getElementById("dataLoadingDIV").style.top = 100;

        if (val == "1")
            parent.document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
        else
            parent.document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
    }

    function CloseLoading()
    {
        DataLoadingnew(0);
    }

</script>
<style type="text/css">
    #tblTimeSlot td
    {
        border: 1px solid #d3d3d3;
    }
    th, td
    {
        font-weight: normal;
        font-size: 9pt;
    }
    .ConfBgColor
    {
        height: 17px;
        z-index: -1;
        position: relative;
        background-color: #ededed;
    }
    .PartyFreeBusyCells
    {
        height: 17px;
        z-index: -1;
        position: relative;
    }
    .HeaderbgColor
    {
        background-color: gray;
        height: 17px;
    }
</style>
<%
    if(Session["userID"] == null)
    {
        Response.Redirect("~/en/genlogin.aspx");
    }
    
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<script type="text/javascript" src="script/CallMonitorJquery/jquery.1.4.2.js" ></script>
<script type="text/javascript" src="script/CallMonitorJquery/jquery.bpopup-0.7.0.min.js"></script>
<script type="text/javascript" src="../<%=Session["language"] %>/script/CallMonitorJquery/MonitorMCU.js"></script>
<script type="text/javascript" src="script/CallMonitorJquery/json2.js" ></script>
<head runat="server">
    <title>Scheduling Assistant</title>
</head>
<body>
    
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" EnableScriptLocalization="true">
        <Scripts>
            <asp:ScriptReference Path="~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>" />
        </Scripts>
    </asp:ScriptManager>
    <input type="hidden" id="hdnConfStartHour" runat="server" />
    <div>
        <table width="100%" border="0" style="margin-top:-10px;margin-left:-5px">
            <tr >                
                <td style="vertical-align: top; padding-left: 0px; width: 100%">
                    <div id="DivTimeSlot" style="position: relative; border: 1px solid #808080;"">
                        <table border="0" width="100%">
                            <tr>
                                <td style="height:28px;">
                                    <div id="DivConfDate" style="padding-bottom: 8px;">
                                        <span id="SpnConfDate" style="font-weight: bold;" runat="server"></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="topDiv" style="position: relative; overflow: hidden;vertical-align:top;" >
                                        <asp:Table runat="server" ID="tblHeaderTimeSlot" border="0" Font-Bold="false" CellPadding="0" 
                                            BorderColor="LightGray" CellSpacing="0" align="left">
                                            <asp:TableHeaderRow>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left; height:17px">12:00</div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;">1:00</div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;">2:00</div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;">3:00</div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;">4:00</div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;">5:00</div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;">6:00</div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;">7:00</div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;">8:00</div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;">9:00</div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;">10:00</div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;">11:00</div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;">12:00</div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;">1:00</div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;">2:00</div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;">3:00</div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;">4:00</div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;">5:00</div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;">6:00</div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;">7:00</div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;">8:00</div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;">9:00</div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;">10.00</div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;">11:00</div></asp:TableHeaderCell>
                                            </asp:TableHeaderRow>
                                            <asp:TableHeaderRow CssClass="HeaderbgColor">
                                                <asp:TableHeaderCell BorderColor="LightGray" BorderWidth="1"></asp:TableHeaderCell>
                                                <asp:TableHeaderCell BorderColor="LightGray" BorderWidth="1"></asp:TableHeaderCell>
                                                <asp:TableHeaderCell BorderColor="LightGray" BorderWidth="1"></asp:TableHeaderCell>
                                                <asp:TableHeaderCell BorderColor="LightGray" BorderWidth="1"></asp:TableHeaderCell>
                                                <asp:TableHeaderCell BorderColor="LightGray" BorderWidth="1"></asp:TableHeaderCell>
                                                <asp:TableHeaderCell BorderColor="LightGray" BorderWidth="1"></asp:TableHeaderCell>
                                                <asp:TableHeaderCell BorderColor="LightGray" BorderWidth="1"></asp:TableHeaderCell>
                                                <asp:TableHeaderCell BorderColor="LightGray" BorderWidth="1"></asp:TableHeaderCell>
                                                <asp:TableHeaderCell BorderColor="LightGray" BorderWidth="1"></asp:TableHeaderCell>
                                                <asp:TableHeaderCell BorderColor="LightGray" BorderWidth="1"></asp:TableHeaderCell>
                                                <asp:TableHeaderCell BorderColor="LightGray" BorderWidth="1"></asp:TableHeaderCell>
                                                <asp:TableHeaderCell BorderColor="LightGray" BorderWidth="1"></asp:TableHeaderCell>
                                                <asp:TableHeaderCell BorderColor="LightGray" BorderWidth="1"></asp:TableHeaderCell>
                                                <asp:TableHeaderCell BorderColor="LightGray" BorderWidth="1"></asp:TableHeaderCell>
                                                <asp:TableHeaderCell BorderColor="LightGray" BorderWidth="1"></asp:TableHeaderCell>
                                                <asp:TableHeaderCell BorderColor="LightGray" BorderWidth="1"></asp:TableHeaderCell>
                                                <asp:TableHeaderCell BorderColor="LightGray" BorderWidth="1"></asp:TableHeaderCell>
                                                <asp:TableHeaderCell BorderColor="LightGray" BorderWidth="1"></asp:TableHeaderCell>
                                                <asp:TableHeaderCell BorderColor="LightGray" BorderWidth="1"></asp:TableHeaderCell>
                                                <asp:TableHeaderCell BorderColor="LightGray" BorderWidth="1"></asp:TableHeaderCell>
                                                <asp:TableHeaderCell BorderColor="LightGray" BorderWidth="1"></asp:TableHeaderCell>
                                                <asp:TableHeaderCell BorderColor="LightGray" BorderWidth="1"></asp:TableHeaderCell>
                                                <asp:TableHeaderCell BorderColor="LightGray" BorderWidth="1"></asp:TableHeaderCell>
                                                <asp:TableHeaderCell BorderColor="LightGray" BorderWidth="1"></asp:TableHeaderCell>
                                            </asp:TableHeaderRow>
                                        </asp:Table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="MainDiv" style="position: relative; height: 251px; margin-top: -5px; overflow-x:auto;overflow-y:auto;"
                                        onscroll="fnUpdateTop(1)">
                                        <asp:Table runat="server" ID="tblTimeSlot" border="0" Font-Bold="false" CellPadding="0"
                                            BorderColor="LightGray" CellSpacing="0" align="left">
                                            <asp:TableHeaderRow>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;"></div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;"></div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;"></div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;"></div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;"></div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;"></div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;"></div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;"></div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;"></div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;"></div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;"></div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;"></div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;"></div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;"></div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;"></div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;"></div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;"></div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;"></div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;"></div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;"></div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;"></div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;"></div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;"></div></asp:TableHeaderCell>
                                                <asp:TableHeaderCell><div style="width:59px; text-align:left;"></div></asp:TableHeaderCell>
                                            </asp:TableHeaderRow>
                                        </asp:Table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
           
        </table>
    </div>
    </form>
</body>
</html>
<script type="text/javascript">

    function fnAssignDivWidth() {
        var mainDiv = document.getElementById("MainDiv");
        var topDiv = document.getElementById("topDiv");
        var DivTimeSlot = document.getElementById("DivTimeSlot");
        var confStartHour = document.getElementById("hdnConfStartHour");
        var DivConfDateCtl = document.getElementById("DivConfDate");

        if (mainDiv) {
            var xWidth = window.screen.width - 486;
            DivTimeSlot.style.width = xWidth + "px";
        }

        if (mainDiv) {
            var xWidth = window.screen.width - 490;
            mainDiv.style.width = xWidth + "px";
            mainDiv.scrollLeft = (confStartHour.value - 1) * 58;
        }
        if (topDiv) {
            var xWidth = window.screen.width - (490 + 15);
            topDiv.style.width = xWidth + "px";
            topDiv.scrollLeft = (confStartHour.value - 1) * 58;
        }
        if(DivConfDateCtl)
        {
            DivConfDateCtl.style.paddingLeft = "40%";
        }
    }

    $(document).ready(function () {
        fnAssignDivWidth();
    });

    function fnUpdateTop(par) {
        document.getElementById("topDiv").scrollLeft = document.getElementById("MainDiv").scrollLeft;

        if (par == 1)
            parent.document.getElementById("UserMainDiv").scrollTop = document.getElementById("MainDiv").scrollTop;
        else
            document.getElementById("MainDiv").scrollTop = parent.document.getElementById("UserMainDiv").scrollTop;
    }

    document.onkeydown = EscClosePopup;
    function EscClosePopup(e) {
        if (e == null)
            var e = window.event;
        if (e.keyCode == 27) {
            window.close();
        }
    }
         
</script>
