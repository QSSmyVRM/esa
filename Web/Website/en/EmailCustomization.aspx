<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_EmailCustomization.EmailCustomization" %>

<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dxHE" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Email Customisation</title>
</head>
<body>
    <form id="form1" runat="server" > <%--ZD 100176--%><%--ZD 101022 - removed dataloading --%>
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <input id="hdnEmailContID" name="hdnEmailContID" runat="server" type="hidden" />
    <input id="hdnEmailLangID" name="hdnEmailLangID" runat="server" type="hidden" />
    <input id="hdnEmailLangName" name="hdnEmailLangName" runat="server" type="hidden" />
    <input id="hdnPlaceHolders" name="hdnPlaceHolders" runat="server" type="hidden" />
    <input id="hdnCreateType" name="hdnCreateType" runat="server" type="hidden" />
    <input id="hdnEmailMode" name="hdnEmailMode" runat="server" type="hidden" />
    <input id="hdnuserid" name="hdnuserid" runat="server" type="hidden" />
    
    <div id="PlaceHolder"  runat="server" align="center" style="top: 250px;left:565px; POSITION: absolute; WIDTH:45%; HEIGHT: 350px;VISIBILITY: visible; Z-INDEX: 3; display:none"> 
      <table cellpadding="2" cellspacing="1"  width="70%" class="tableBody" align="center" bgcolor='#E1E1E1'><%--ZD 100426--%>
         <tr>
            <td class="subtitleblueblodtext" align="center" colspan="2"><asp:Literal Text="<%$ Resources:WebResources, EmailCustomization_PlaceholderDes%>" runat="server"></asp:Literal></td>            
         </tr>
         <tr>
            <td width="9%"></td>
           <td align="center">
               <div style="width: 100%;height: 350px;overflow: auto;" id="dd" runat="server" >
                    <asp:DataGrid ID="dgPlaceHolders" runat="server" AutoGenerateColumns="False" CellPadding="2" GridLines="None" AllowSorting="true" 
                          BorderStyle="solid" BorderWidth="0" ShowFooter="False" 
                         Width="100%" Visible="true" style="border-collapse:separate" >
                        <SelectedItemStyle  CssClass="tableBody"/>
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody"  />                        
                        <FooterStyle CssClass="tableBody"/>
                        <Columns>
                            <asp:BoundColumn DataField="PlaceHolderID"  HeaderStyle-Width="5%" ></asp:BoundColumn>
                            <asp:BoundColumn DataField="Description" ItemStyle-CssClass="tableBody" ></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
           </td>
          </tr>
          <tr>
           <td align="center" colspan="2">
              <asp:Button id="BtnClose" text="<%$ Resources:WebResources, EmailCustomization_BtnClose%>" cssclass="altShortBlueButtonFormat" runat="server" onclientclick="javascript:return fnShowHide('0')"></asp:Button>
           </td>
          </tr>
        </table>
    </div>
    <div>
    <h3><span id="spnHeader" runat="server"></span></h3><!-- FB 2570 -->
     <table width="100%" border="0" cellpadding="5">
        <tr>
        <%--ZD 104606 - Fixed Compass Group During upgrade Start--%>
        <%--<td id="tdEmail" style="width:15%"></td>--%> <!-- FB 2050 --> 
        <td></td><%--ZD 104628--%>
        <td align="center" id="tdDiv" colspan="2" style="width:85%"> <!-- FB 2050 -->
            <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError" Visible="False"></asp:Label><br />  
             <div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display:none">
                 <img border='0' src='image/wait1.gif' alt='Loading..' />
             </div><%--ZD 100678--%>
        </td>
       <%--<td style="width:30%"></td>--%> <!-- FB 2050 -->
       <%--ZD 104606 - Fixed Compass Group During upgrade End--%>
        </tr>
        <tr>
            <td valign="top" align="left" class="blackblodtext" style="width:15%"> <!-- FB 2050 --><%--ZD 104606--%>
                <asp:Literal Text="<%$ Resources:WebResources, EmailCustomization_EmailLanguage%>" runat="server"></asp:Literal>
            </td>
            <td align="left" colspan="2" style="width:85%"><%--ZD 104606--%>
                <asp:TextBox ID="txtEmailLang" runat="server" Width="15%" CssClass="altText"></asp:TextBox>
            </td>
        </tr>
        <tr>           
            <td align="left" class="blackblodtext" valign="top"><asp:Literal Text="<%$ Resources:WebResources, EmailCustomization_EmailCategory%>" runat="server"></asp:Literal></td>
            <td colspan="2">
                <table width="100%" border="0" cellspacing="0">
                   <tr>            
                        <td align="left" valign="top" width="18%">
                            <asp:DropDownList ID="lstEmailCategory" runat="server" AutoPostBack="true" CssClass="altSelectFormat" OnSelectedIndexChanged="BindMailTypes" onchange="JavaScript:modedisplay(); DataLoading(1);" ><%--ZD 100176--%> 
                                <asp:ListItem Value="1" Text="<%$ Resources:WebResources, General%>"></asp:ListItem>
                                <asp:ListItem Value="2" Text="<%$ Resources:WebResources, PreConfScheduling%>"></asp:ListItem>
                                <asp:ListItem Value="3" Text="<%$ Resources:WebResources, ApprovalDenialMails%>"></asp:ListItem>
                                <asp:ListItem Value="4" Text="<%$ Resources:WebResources, ConfirmationMails%>"></asp:ListItem>
                                <asp:ListItem Value="5" Text="<%$ Resources:WebResources, PostConfScheduling%>"></asp:ListItem>
                                <asp:ListItem Value="7" Text="<%$ Resources:WebResources, AlternateApprovalMails%>"></asp:ListItem><%--ZD 100642--%>
                            </asp:DropDownList>
                        </td>
                        <td valign="top" class="blackblodtext" width="12%" align="left"><asp:Literal Text="<%$ Resources:WebResources, EmailCustomization_EmailType%>" runat="server"></asp:Literal></td>
                        <td width="25%" align="left" valign="top"> <%--FB 1948--%>
                            <asp:DropDownList ID="lstEmailType" runat="server" AutoPostBack="true" CssClass="altSelectFormat" OnSelectedIndexChanged="GetEmailContent" DataTextField="emailtype" DataValueField="emailtypeid" onchange="JavaScript:defaultmode(); DataLoading(1);" Width="90%"></asp:DropDownList> <%--ZD 100176--%> 
                        </td>
                        <td valign="top" id="tdConfMode">
                            <table width="100%" id="tableConfMode" cellpadding="0" cellspacing="0" border="0">
                              <tr>
                                <td valign="top" width="45%" class="blackblodtext" align="left"><asp:Literal Text="<%$ Resources:WebResources, EmailCustomization_ConferenceMode%>" runat="server"></asp:Literal></td>
                                <td valign="top"> <%--FB 1948--%>
                                    <asp:DropDownList ID="lstEmailMode" runat="server" AutoPostBack="true" CssClass="altSelectFormat" onchange="JavaScript:setconfmode('1'); DataLoading(1);" OnSelectedIndexChanged="GetEmailContent"> <%--ZD 100176--%> 
                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Create%>"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="<%$ Resources:WebResources, Edit%>"></asp:ListItem>
                                    </asp:DropDownList>
                                    
                                    <asp:DropDownList ID="lstAllEmailMode" runat="server" AutoPostBack="true" onchange="JavaScript:setconfmode('2')" CssClass="altSelectFormat" OnSelectedIndexChanged="GetEmailContent">
                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Create%>"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="<%$ Resources:WebResources, Edit%>"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="<%$ Resources:WebResources, Delete%>"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                              </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>        
        <tr>
            <td valign="top"  align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, EmailCustomization_EmailSubject%>" runat="server"></asp:Literal></td>
            <td colspan="2"> <%--ZD 104556  Starts--%>
            <table width="100%" border="0" cellspacing="0">
                <tr>            
                    <td align="left" valign="top" width="18%">
                        <asp:TextBox ID="txtEmailSubject" runat="server" CssClass="altText"> </asp:TextBox>
                    </td>
                    <td valign="top" class="blackblodtext" width="12%" align="left" id="tdConfLbl" runat="server">
                        <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, ConferenceSetup_Field3%>" runat="server"></asp:Literal>
                    </td>
                    <td align="left" valign="top" id="tdconftype" runat="server"> 
                        <asp:DropDownList ID="lstConfType" runat="server" AutoPostBack="true" CssClass="altSelectFormat" OnSelectedIndexChanged="GetEmailContent" onchange="JavaScript:ConfTypedisplay(); DataLoading(1);" >
                         <asp:ListItem Selected="True" Value="6" Text="<%$ Resources:WebResources, AudioOnly1%>"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="<%$ Resources:WebResources, AudioVideo%>"></asp:ListItem>
                                                    <asp:ListItem Value="4" Text="<%$ Resources:WebResources, ManageConference_pttopt%>"></asp:ListItem>
                                                    <asp:ListItem Value="7" Text="<%$ Resources:WebResources, RoomOnly1%>"></asp:ListItem>
                                                    <asp:ListItem Value="8" Text="<%$ Resources:WebResources, Hotdesking%>"></asp:ListItem>
                                                    <asp:ListItem Value="9" Text="<%$ Resources:WebResources, OBTP%>"></asp:ListItem>
                        </asp:DropDownList> 
                    </td>
                    <td width="45%" align="left" valign="top" >
                        <asp:LinkButton id="lnkShow" text="<%$ Resources:WebResources, EmailCustomization_lnkShow%>" runat="server" onclientclick="javascript:return fnShowHide('1')"></asp:LinkButton>
                    </td> 
                </tr>
            </table> <%--ZD 104556  Ends--%>
            </td>
            
        </tr>       
        <tr>
            <td valign="top" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, EmailCustomization_EmailBody%>" runat="server"></asp:Literal></td>
            <td align="left" valign='top' colspan="2"> <!-- FB 2050 -->
                <dxHE:ASPxHtmlEditor AccessibilityCompliant="true" ID="dxHTMLEditor" runat="server" Width="100%">
                <ClientSideEvents LostFocus="function(s,e){setTimeout('fnSetFocus();',100);}" /><%--ZD 100420 ZD 100369 508 Issue--%>
                </dxHE:ASPxHtmlEditor>
           </td>
        </tr>
        <tr>
          <td class="blackblodtext"></td>
          <td style="color: Red;" align="left" colspan="2"> <!-- FB 2050 --><asp:Literal Text="<%$ Resources:WebResources, EmailCustomization_NotePleasedon%>" runat="server"></asp:Literal></td>
        </tr>
        <tr id="icalNote" runat="server"> <%--ZD 101990--%>
          <td class="blackblodtext"></td>
          <td style="color: Red;" align="left" colspan="2"> <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, IcalCustMesasge%>" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">
                <table width="100%" align="center">
                    <tr>
                    <%--Added for SoftEdge Button--%>
                    <td>
                    <input type='submit' name='SoftEdgeTest1' tabindex='-1' style='max-height:0px;max-width:0px;height:0px;width:0px;background-color:Transparent;border:None;'/></td><%--100420--%><%--100369--%>
                        <td align="center">
                            <asp:Button Text="<%$ Resources:WebResources, EmailCustomization_btnSubmit%>" runat="server" ID="btnSubmit"  OnClick="SetEmailContent" Width="100pt" /> <%--FB 2565 FB 2796--%>
                            <asp:Button Text="<%$ Resources:WebResources, EmailCustomization_btnCancel%>" runat="server" ID="btnCancel" CssClass="altMedium0BlueButtonFormat" OnClick="RedirectToTargetPage" OnClientClick="javascript:DataLoading(1);"/><%--ZD 100176--%> 
                        </td>
                        <td width="2%"></td>
                        
                    </tr>
                </table>
            </td>
        </tr>
    </table>
                
    </div>
    </form>
</body>
</html>
<script type="text/javascript">
//ZD 100604 start
var img = new Image();
img.src = "../en/image/wait1.gif";
//ZD 100604 End
function changeFocus()
{
  // FB 2050
  var obj1 = document.getElementById("txtEmailSubject"); // For EmailCustomization Page
  if(obj1 != null)
  obj1.focus();
}
window.onload = changeFocus;
</script>
 <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<script language="JavaScript" type="text/javascript">
    modedisplay();
    ConfTypedisplay(); //ZD 104556
 function modedisplay()
  {
     var mode = document.getElementById("lstEmailMode");
     var modeall = document.getElementById("lstAllEmailMode");
     var hdnconfmode = document.getElementById("hdnEmailMode");
     
     document.getElementById("tableConfMode").style.display = "none";
     if (document.getElementById("lstEmailCategory").value == "2") 
     {
         document.getElementById("tableConfMode").style.display = "block";
         mode.style.display = "block";
         modeall.style.display = "none";
         hdnconfmode.value = "1";
     }
     else if (document.getElementById("lstEmailCategory").value == "4")
      {
         document.getElementById("tableConfMode").style.display = "block";
         mode.style.display = "none";
         modeall.style.display = "block";
         hdnconfmode.value = "1";
     }
 }
 //ZD 104556 Strats
 function ConfTypedisplay() {
     var type = document.getElementById("lstEmailType");
     var hdnconfmode = document.getElementById("lstConfType");

     document.getElementById("tdconftype").style.visibility = 'hidden';
     document.getElementById("tdConfLbl").style.visibility = 'hidden';
     if (document.getElementById("lstEmailCategory").value == "4" && (type.value == "11" || type.value == "12" || type.value == "13" || type.value == "26")) {
         document.getElementById("tdconftype").style.visibility = 'visible';
         document.getElementById("tdConfLbl").style.visibility = 'visible';
     }
    else
    hdnconfmode.value = "6";
 }
 //ZD 104556 Ends
 function setconfmode()
 {
    var args=setconfmode.arguments;
    var mode = document.getElementById("lstEmailMode");
    var modeall = document.getElementById("lstAllEmailMode");
    var hdnconfmode = document.getElementById("hdnEmailMode");
    
    if(args[0] == '1')
    {
        hdnconfmode.value = '';
        hdnconfmode.value = mode.value;
    }
    else
    {
        hdnconfmode.value = '';
        hdnconfmode.value = modeall.value;
    }
 }
  
 function defaultmode() 
 {
     document.getElementById("lstAllEmailMode").value = "1";
     document.getElementById("lstEmailMode").value = "1";
     document.getElementById("lstConfType").value = "6"; //ZD 104556
 }
 
function fnShowHide(arg) {
    //f(document.getElementById("lnkShow").disabled == false) { //FB 2768
      if (arg == '1')
          document.getElementById("PlaceHolder").style.display = 'block';
      else if (arg == '0')
          document.getElementById("PlaceHolder").style.display = 'none';
    //}
    return false; 
  }

 if (document.getElementById('dxHTMLEditor_TD_T0_DXI15_I')) //To hide ImageButton in Control
     document.getElementById('dxHTMLEditor_TD_T0_DXI15_I').style.display = 'none'

 var btnsubmit = document.getElementById("btnSubmit");
 var lnkshow = document.getElementById("lnkShow");
 
 if (document.getElementById("lstEmailCategory").value == "4")
 {
     var modeall = document.getElementById("lstAllEmailMode");
     var mailtype = document.getElementById("lstEmailType");
     //ZD 101990 Starts
     document.getElementById("tableConfMode").style.display = "block";
     if (mailtype.value == "26")
         document.getElementById("tableConfMode").style.display = "none";
     //ZD 101990 Ends

     if (mailtype.value == "10" || mailtype.value == "25") //MCU Alert
     {
         if (modeall.value == "3" )
         {
             btnsubmit.disabled = true;
             lnkshow.disabled = true;
             //FB 2298  - Starts
             btnsubmit.style.color = "gray";
             lnkshow.style.color = "gray";
         }
         else  
             lnkshow.disabled = false;
     }
    else 
        lnkshow.disabled = false; 
      //FB 2298 //FB 2995 - End
      
 }
 else
 {
    //FB 2670
    if("<%=isVis%>" == "true")
    {
        btnsubmit.disabled = false;
        lnkshow.disabled = false;
    }
 }
 //ZD 100176 start
 function DataLoading(val) {
     if (val == "1")
         document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
     else
         document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
 }
 //ZD 100176 End    
</script>

<%--ZD 100428 START--%>
<script language="javascript" type="text/javascript">

    document.onkeydown = function(evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
            if (document.getElementById("btnGoBack") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnGoBack").click();
                    return false;
                }
            }
        }
        fnOnKeyDown(evt);
    };
    
    function EscClosePopup() {
        if (document.getElementById('PlaceHolder') != null) {
            document.getElementById('PlaceHolder').style.display = "none";

        }
    }
    //ZD 100420 - Start
    function fnFocusMenu() {
        if (document.getElementById("menuHome") != null)
            document.getElementById("menuHome").focus();
        else if (document.getElementById("menuHomeBlue") != null)
            document.getElementById("menuHomeBlue").focus();
        else
            document.getElementById("menuHomeRed").focus();
    }
    var glob = false;
    function fnSetFocus() {
        if (navigator.userAgent.indexOf('Trident') > -1 && glob)
            document.getElementById("dxHTMLEditor_TC_T1T").focus();
        if (navigator.userAgent.indexOf('Chrome') > -1 && !glob) { // ZD 100369 508 Issue
            if(document.activeElement.tagName == "A")
                setTimeout('fnFocusMenu();', 100);
        }
        glob = true;
    }
    document.getElementById("txtEmailLang").setAttribute("onfocus","window.scrollTo(0,0)")
    //ZD 100420 - End

    //ZD 104606 - Fixed Compass Group During upgrade Start
//    if (navigator.userAgent.indexOf('Trident') > -1) {
//        document.getElementById('tdEmail').style.width = "20%";
//        document.getElementById('tdDiv').style.width = "50%";        
    //    }
    //ZD 104606 - Fixed Compass Group During upgrade End
</script>
