/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;
using cryptography;


namespace myVRM.DataLayer
{
	/// <summary>
	/// Summary description for MCU_List_D and related tables
	/// </summary>
    ///
    public class vrmLDAPConfig
	{
        
        #region Private Internal Members

        private string m_serveraddress;
        private string m_login;
        private string m_password;
		private int m_port;
        private int m_timeout;
        private int m_schedule, m_AuthType;//FB 2993 LDAP
        private DateTime m_SyncTime;
        private DateTime m_scheduleTime;
        private string m_SearchFilter;
        private string m_LoginKey;
		private string m_domainPrefix;
        private string m_scheduleDays;
        private int m_UID; //FB 2027 SetSuperAdmin
        private int m_connPort = 389;
        private int m_sessionTimeout = 20;
        private int m_scheduleInterval = 1440; // 1 day
        private int m_EnableDomain, m_EnableSSOMode, m_TenantOption, m_OrgId; //ZD 101443-LDAP
        #endregion
		
		#region Public Properties

        public string serveraddress 
		{
            get { return m_serveraddress; }
            set { m_serveraddress = value; }
		}
        public string login 
		{
            get { return m_login; }
            set { m_login = value; }
		}
        public string password 
		{
            get { return m_password; }
            set { m_password = value; }
		}
        public int port 
		{
            get { return m_port; }
            set { m_port = value; }
		}
        public int timeout 
		{
            get { return m_timeout; }
            set { m_timeout = value; }
		}
        public int schedule
        {
            get { return m_schedule; }
            set { m_schedule = value; }
        }
        public DateTime SyncTime 
		{
            get { return m_SyncTime; }
            set { m_SyncTime = value; }
		}
        public DateTime scheduleTime
        {
            get { return m_scheduleTime; }
            set { m_scheduleTime = value; }
        }
        public string SearchFilter 
		{
            get { return m_SearchFilter; }
            set { m_SearchFilter = value; }
		}
        public string LoginKey
		{
            get { return m_LoginKey; }
            set { m_LoginKey = value; }
		}
        public string domainPrefix 
		{
            get { return m_domainPrefix; }
            set { m_domainPrefix = value; }
		}
        public string scheduleDays 
		{
            get { return m_scheduleDays; }
            set { m_scheduleDays = value; }
		}

        public int connPort
        {
            get { return m_connPort; }
            set { m_connPort = value; }
        }
        public int sessionTimeout
        {
            get { return m_sessionTimeout; }
            set { m_sessionTimeout = value; }
        }
        public int scheduleInterval
        {
            get { return m_scheduleInterval; }
            set { m_scheduleInterval = value; }
        }
        private string m_serverPassword;
        public string serverPassword 
		{
            get{return m_serverPassword;}
            set{m_serverPassword = value;}
        }
        private string m_serverLogin;
        public string serverLogin
        {
            get { return m_serverLogin; }
            set { m_serverLogin = value; }
        }
        //FB 2993 LDAP START
        public int AuthType
        {
            get { return m_AuthType; }
            set { m_AuthType = value; }
        }
        //FB 2993 LDAP END
        //FB 2027 SetSuperAdmin start
        public int UID
        {
            get { return m_UID; }
            set { m_UID = value; }
        }
        //FB 2027 SetSuperAdmin end

        //ZD 101443-LDAP Starts
        public int EnableDomain
        {
            get { return m_EnableDomain; }
            set { m_EnableDomain = value; }
        }
        public int TenantOption
        {
            get { return m_TenantOption; }
            set { m_TenantOption = value; }
        }
        public int EnableSSOMode
        {
            get { return m_EnableSSOMode; }
            set { m_EnableSSOMode = value; }
        }
        public int OrgId
        {
            get { return m_OrgId; }
            set { m_OrgId = value; }
        }
        //ZD 101443-LDAP End
        #endregion

      
        public vrmLDAPConfig()
        {
         
        }
	}
    //ZD 101525
    public class vrmLDAPGroups
    {

        #region Private Internal Members

        private string m_GroupName;
        private int m_OrgId;
        private int m_RoleOrTemplateId, m_Origin;//ZD 102052
        private int m_Deleted;
        private int m_UID, m_ModifiedUserId;
        private DateTime m_LastModifiedDate = DateTime.UtcNow;
        #endregion

        #region Public Properties

        public string GroupName
        {
            get { return m_GroupName; }
            set { m_GroupName = value; }
        }
        public int OrgId
        {
            get { return m_OrgId; }
            set { m_OrgId = value; }
        }
		//ZD 102052 Starts
        public int RoleOrTemplateId
        {
            get { return m_RoleOrTemplateId; }
            set { m_RoleOrTemplateId = value; }
        }
		//ZD 102052 End
        public int Deleted
        {
            get { return m_Deleted; }
            set { m_Deleted = value; }
        }
        public int UID
        {
            get { return m_UID; }
            set { m_UID = value; }
        }
        public int ModifiedUserId
        {
            get { return m_ModifiedUserId; }
            set { m_ModifiedUserId = value; }
        }
        public DateTime LastModifiedDate
        {
            get { return m_LastModifiedDate; }
            set { m_LastModifiedDate = value; }
        }
		//ZD 102052 Starts
        public int Origin
        {
            get { return m_Origin; }
            set { m_Origin = value; }
        }
		//ZD 102052 End
        #endregion


        public vrmLDAPGroups()
        {

        }
    }
}
